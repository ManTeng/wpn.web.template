﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="Bee.Core" %>
<%@ Import Namespace="WPN" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        CoreSrv.Init();
        CoreSrv.Plugin.Enable("WPN", "WPN", "WPN.PluginActivator");
        EventAction.Add("CoreSrv_AppStart", new Action(() =>
        {
            CoreSrv.Plugin.Enable("WPN.DAL.Sql", "WPN.DAL.Sql", "WPN.DAL.PluginActivator");
        }));
        EventAction.Apply("CoreSrv_AppStart");
        EventAction.Apply("Plugin_Run_AppStart");
        
    }

    void Application_End(object sender, EventArgs e)
    {
        EventAction.Apply("CoreSrv_AppEnd");

    }

    void Application_Error(object sender, EventArgs e)
    {
        EventAction.Apply("CoreSrv_OnError", sender, e);
    }

    void Session_Start(object sender, EventArgs e)
    {
        EventAction.Apply("CoreSrv_SessionStart", sender, e);
    }

    void Session_End(object sender, EventArgs e)
    {
        EventAction.Apply("CoreSrv_SessionEnd",sender,e);
    }

</script>
