﻿[Hash]
* Each time, when the application is installed, a new GUID will be generated and wrote in WPNConfig.cshtml. The default hash method
will uses the GUID as a salt.
* You could overwrite this in application level by hooking filter Hash_String.

[Translation]
* Make sure you are saving the PO file with encoding UTF-8.

[Note]
* Do not use Session unless you put it in a server(db/cache), because the ASP.Net web application will be recycled every fix hours
* Overwrites any binary under BIN directory will cause the application to be restarted