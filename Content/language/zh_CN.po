﻿msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2015-10-14 01:23:36+0800\n"
"PO-Revision-Date: 2015-10-14 01:33+0800\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.5\n"
"Language: zh_CN\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: ../zulian/WPN/Utl.cs:14
msgid "Posted"
msgstr "已发布"

#: ../zulian/WPN/Utl.cs:16
msgid "Draft"
msgstr "草稿"

#: ../zulian/WPN/Utl.cs:18
msgid "Trash"
msgstr "回收站"

#: ../zulian/WPN/Utl.cs:20
msgid "Invalid"
msgstr "无效"

#: ../zulian/WPN/WPNBuildManager.cs:89
#, csharp-format
msgid "The source is not compiled for {0}"
msgstr "文件{0}未被编译"

#: ../zulian/WPN.WebSite/wpnAdmin/Index.cshtml:4
msgid "Dashboard"
msgstr "控制面板"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:7
msgid "Page"
msgstr "页面"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:18
#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:19
#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:109
msgid "Pages"
msgstr "页面"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:18
msgid "New Page"
msgstr "新页面"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:19
#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:30
msgid "Home"
msgstr "首页"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:26
#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:111
msgid "All Pages"
msgstr "所有页面"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:31
msgid "Title"
msgstr "标题"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:32
#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:42
msgid "Author"
msgstr "作者"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:32
msgid "Category"
msgstr "分类"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:32
msgid "Tag"
msgstr "标签"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:32
msgid "Comments"
msgstr "评论"

#: ../zulian/WPN.WebSite/wpnAdmin/Pages.cshtml:32
msgid "Date"
msgstr "日期"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:6
msgid "Plugin"
msgstr "插件"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:29
#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:30
#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:117
msgid "Plugins"
msgstr "插件"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:29
#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:72
#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:121
msgid "Install Plugins"
msgstr "安装插件"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:37
#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:120
msgid "All Plugins"
msgstr "所有插件"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:42
msgid "Plugin Name"
msgstr "插件名称"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:42
msgid "Version"
msgstr "版本"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:42
msgid "Description"
msgstr "描述"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:42
msgid "Action"
msgstr "操作"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:58
msgid "DeActive"
msgstr "禁用"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:62
msgid "Active"
msgstr "启用"

#: ../zulian/WPN.WebSite/wpnAdmin/Plugins.cshtml:64
msgid "Deinstall"
msgstr "卸载"

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:23
msgid "Singing in...."
msgstr "正在登入......."

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:36
msgid "Log in"
msgstr "登录"

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:63
msgid "Sign in"
msgstr "登录"

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:77
msgid "Remember Me"
msgstr "记住我"

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:82
msgid "Sign In"
msgstr "登录"

#: ../zulian/WPN.WebSite/wpnAdmin/WPNLogin.cshtml:91
msgid "Register a new membership"
msgstr "新用户注册"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:107
msgid "MAIN NAVIGATION"
msgstr "主导航栏"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:112
msgid "New"
msgstr "新建"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:128
msgid "Themes"
msgstr "主题"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:132
msgid "All Themes"
msgstr "所有主题"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:133
msgid "Install Themes"
msgstr "安装主题"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:138
msgid "Settings"
msgstr "设置"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:141
msgid "Profile Settings"
msgstr "个人设置"

#: ../zulian/WPN.WebSite/wpnAdmin/_Layout.cshtml:142
msgid "System Settings"
msgstr "系统设置"
